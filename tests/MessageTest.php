<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams\Tests;

use Monolog\DateTimeImmutable;
use Monolog\Level;
use Monolog\LogRecord;
use SlyFoxCreative\Logging\MicrosoftTeams\Message;

class MessageTest extends TestCase
{
    public function testMessage(): void
    {
        $record = new LogRecord(
            message: 'This is a test message.',
            level: Level::Debug,
            channel: 'Test',
            datetime: new DateTimeImmutable(true),
        );

        $expected = [
            '@type' => 'MessageCard',
            '@context' => 'https://schema.org/extensions',
            'title' => 'Log Test',
            'text' => 'This is a test message.',
            'sections' => [
                [
                    'facts' => [
                        ['name' => 'Level', 'value' => 'DEBUG'],
                    ],
                ],
            ],
        ];

        $message = new Message($record, 'Log Test', null);

        self::assertSame($expected, $message->json());
    }

    public function testContext(): void
    {
        $record = new LogRecord(
            message: 'This is a test message.',
            level: Level::Debug,
            channel: 'Test',
            datetime: new DateTimeImmutable(true),
            context: [
                'test' => 1,
            ],
        );

        $expected = [
            '@type' => 'MessageCard',
            '@context' => 'https://schema.org/extensions',
            'title' => 'Log Test',
            'text' => 'This is a test message.',
            'sections' => [
                [
                    'facts' => [
                        ['name' => 'Level', 'value' => 'DEBUG'],
                    ],
                ],
                [
                    'facts' => [
                        ['name' => 'test', 'value' => 1],
                    ],
                ],
            ],
        ];

        $message = new Message($record, 'Log Test', null);

        self::assertSame($expected, $message->json());
    }

    public function testException(): void
    {
        $record = new LogRecord(
            message: 'Error message.',
            level: Level::Debug,
            channel: 'Test',
            datetime: new DateTimeImmutable(true),
            context: [
                'exception' => new TestException(),
            ],
        );

        $expected = [
            '@type' => 'MessageCard',
            '@context' => 'https://schema.org/extensions',
            'title' => 'Log Test',
            'text' => 'Error message.',
            'sections' => [
                [
                    'facts' => [
                        ['name' => 'Level', 'value' => 'DEBUG'],
                        [
                            'name' => 'Exception',
                            'value' => 'SlyFoxCreative\Logging\MicrosoftTeams\Tests\TestException',
                        ],
                        ['name' => 'File', 'value' => 'tests/Test.php'],
                        ['name' => 'Line', 'value' => 1],
                    ],
                ],
                [
                    'text' => "[NONE]: test()\n\ntests/file.php(1): test()",
                ],
            ],
        ];

        $directory = getcwd();
        if ($directory === false) {
            throw new \Exception('Failed to get current working directory');
        }

        $message = new Message($record, 'Log Test', $directory);

        self::assertSame($expected, $message->json());
    }
}
