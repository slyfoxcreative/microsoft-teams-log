<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams\Tests;

use BlastCloud\Guzzler\UsesGuzzler;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use UsesGuzzler;
}
