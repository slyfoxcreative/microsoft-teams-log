<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams\Tests;

class TestException
{
    public function getMessage(): string
    {
        return 'Error message.';
    }

    public function getFile(): string
    {
        return getcwd() . '/tests/Test.php';
    }

    public function getLine(): int
    {
        return 1;
    }

    /** @return array<int, Call> */
    public function getTrace(): array
    {
        return [
            [
                'function' => 'test',
                'args' => [],
            ],
            [
                'file' => getcwd() . '/tests/file.php',
                'line' => 1,
                'function' => 'test',
                'args' => [],
            ],
        ];
    }
}
