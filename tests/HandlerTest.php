<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams\Tests;

use GuzzleHttp\Psr7\Response;
use Monolog\Logger;
use SlyFoxCreative\Logging\MicrosoftTeams\Handler;

class HandlerTest extends TestCase
{
    protected string $directory;

    protected function setUp(): void
    {
        parent::setUp();

        $directory = getcwd();
        if ($directory === false) {
            throw new \Exception('Failed to get current working directory');
        }
        $this->directory = $directory;
    }

    public function testMessage(): void
    {
        $this
            ->guzzler
            ->expects($this->once())
            ->post($_ENV['WEBHOOK_URL'])
            ->withHeaders([
                'Content-Type' => 'application/json',
            ])
            ->withJson([
                '@type' => 'MessageCard',
                '@context' => 'https://schema.org/extensions',
                'title' => 'Log Test',
                'text' => 'This is a test message.',
                'sections' => [
                    [
                        'facts' => [
                            ['name' => 'Level', 'value' => 'DEBUG'],
                        ],
                    ],
                ],
            ])
            ->willRespond(new Response(200, [], '1'))
        ;

        $handler = new Handler(
            $_ENV['WEBHOOK_URL'],
            'Log Test',
            $this->directory,
            $this->guzzler->getClient(),
        );
        $log = new Logger('Test');
        $log->pushHandler($handler);

        $log->debug('This is a test message.');
    }

    public function testContext(): void
    {
        $this
            ->guzzler
            ->expects($this->once())
            ->post($_ENV['WEBHOOK_URL'])
            ->withHeaders([
                'Content-Type' => 'application/json',
            ])
            ->withJson([
                '@type' => 'MessageCard',
                '@context' => 'https://schema.org/extensions',
                'title' => 'Log Test',
                'text' => 'This is a test message.',
                'sections' => [
                    [
                        'facts' => [
                            ['name' => 'Level', 'value' => 'DEBUG'],
                        ],
                    ],
                    [
                        'facts' => [
                            ['name' => 'test', 'value' => 1],
                        ],
                    ],
                ],
            ])
            ->willRespond(new Response(200, [], '1'))
        ;

        $handler = new Handler(
            $_ENV['WEBHOOK_URL'],
            'Log Test',
            $this->directory,
            $this->guzzler->getClient(),
        );
        $log = new Logger('Test');
        $log->pushHandler($handler);

        $log->debug('This is a test message.', ['test' => 1]);
    }

    public function testException(): void
    {
        $this
            ->guzzler
            ->expects($this->once())
            ->post($_ENV['WEBHOOK_URL'])
            ->withHeaders([
                'Content-Type' => 'application/json',
            ])
            ->withJson([
                '@type' => 'MessageCard',
                '@context' => 'https://schema.org/extensions',
                'title' => 'Log Test',
                'text' => 'Error message.',
                'sections' => [
                    [
                        'facts' => [
                            ['name' => 'Level', 'value' => 'DEBUG'],
                            [
                                'name' => 'Exception',
                                'value' => 'SlyFoxCreative\Logging\MicrosoftTeams\Tests\TestException',
                            ],
                            ['name' => 'File', 'value' => 'tests/Test.php'],
                            ['name' => 'Line', 'value' => 1],
                        ],
                    ],
                    [
                        'text' => "[NONE]: test()\n\ntests/file.php(1): test()",
                    ],
                ],
            ])
            ->willRespond(new Response(200, [], '1'))
        ;

        $handler = new Handler(
            $_ENV['WEBHOOK_URL'],
            'Log Test',
            $this->directory,
            $this->guzzler->getClient(),
        );
        $log = new Logger('Test');
        $log->pushHandler($handler);

        $log->debug('Error message.', ['exception' => new TestException()]);
    }
}
