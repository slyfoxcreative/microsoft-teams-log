<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams;

use GuzzleHttp\Client;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Level;
use Monolog\LogRecord;
use Psr\Log\LogLevel;

/**
 * Sends log messages to Microsoft Teams.
 */
class Handler extends AbstractProcessingHandler
{
    /**
     * @param  string  $url  The Teams webhook URL
     * @param  string  $title  The Teams message title
     * @param  ?string  $basePath  The base path to strip from file paths in
     *                             exception stack traces
     * @param  Client  $client  The Guzzle HTTP client to use for sending
     *                          requests to Teams
     * @param  int|Level|string  $level  The minimum logging level at which this
     *                                   handler will be triggered
     * @param  bool  $bubble  Whether the messages that are handled can
     *                        bubble up the stack or not
     *
     * @phpstan-param value-of<Level::VALUES>|value-of<Level::NAMES>|Level|LogLevel::* $level
     */
    public function __construct(
        private string $url,
        private string $title,
        private ?string $basePath = null,
        private Client $client = new Client(),
        int|string|Level $level = Level::Debug,
        bool $bubble = true,
    ) {
        parent::__construct($level, $bubble);
    }

    /**
     * Send the log message to Teams.
     */
    protected function write(LogRecord $record): void
    {
        $message = new Message($record, $this->title, $this->basePath);
        $this->client->request('POST', $this->url, ['json' => $message->json()]);
    }
}
