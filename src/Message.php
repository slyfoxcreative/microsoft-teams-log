<?php

declare(strict_types=1);

namespace SlyFoxCreative\Logging\MicrosoftTeams;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Monolog\LogRecord;

/**
 * Sends log messages to Microsoft Teams.
 */
class Message
{
    /**
     * @param  LogRecord  $record  The log message
     * @param  string  $title  The Teams message title
     * @param  ?string  $basePath  The base path to strip from file paths in
     *                             exception stack traces
     */
    public function __construct(
        private LogRecord $record,
        private string $title,
        private ?string $basePath,
    ) {}

    /**
     * Return an array of data to be sent as JSON to the Teams Webhook.
     *
     * @return array<string, array<int, FactSection|TextSection>|string>
     */
    public function json(): array
    {
        /** @var Collection<int, Fact> */
        $info = collect([
            ['name' => 'Level', 'value' => $this->record->level->getName()],
        ]);

        $exception = $this->record->context['exception'] ?? null;

        if (is_object($exception)) {
            $info->push(['name' => 'Exception', 'value' => get_class($exception)]);
            if (method_exists($exception, 'getFile')) {
                $info->push(['name' => 'File', 'value' => $this->unprefixPath($exception->getFile())]);
            }
            if (method_exists($exception, 'getLine')) {
                $info->push(['name' => 'Line', 'value' => $exception->getLine()]);
            }
        }

        /** @var Collection<int, FactSection|TextSection> */
        $sections = collect([
            ['facts' => $info->toArray()],
        ]);

        /** @var array<int, Fact> */
        $context = collect($this->record->context)
            ->except('exception')
            ->map(fn($value, $key) => ['name' => $key, 'value' => $value])
            ->values()
            ->toArray()
        ;

        if (count($context) > 0) {
            $sections->push(['facts' => $context]);
        }

        if (is_object($exception) && method_exists($exception, 'getTrace')) {
            $sections->push(['text' => $this->trace($exception->getTrace())]);
        }

        /** @var array<int, FactSection|TextSection> */
        $sections = $sections->toArray();

        return [
            '@type' => 'MessageCard',
            '@context' => 'https://schema.org/extensions',
            'title' => $this->title,
            'text' => $this->escapeMarkdown($this->record->message),
            'sections' => $sections,
        ];
    }

    /**
     * Escape Markdown special characters.
     */
    private function escapeMarkdown(string $text): string
    {
        $escaped = Str::of($text)->replaceMatches('/([\`*_{}[]()#+-.!])/', fn($m) => "\\{$m[1]}");

        return (string) $escaped;
    }

    /**
     * Remove the app's base-directory prefix from a file path.
     */
    private function unprefixPath(string $path): string
    {
        if (is_null($this->basePath)) {
            return $path;
        }

        return Str::after($path, "{$this->basePath}/");
    }

    /**
     * Make a stack trace formatted with Markdown.
     *
     * @param  array<int, Call>  $trace
     */
    private function trace(array $trace): string
    {
        return collect($trace)
            ->map(function ($call) {
                $args = collect($call['args'] ?? [])->implode(', ');
                $file = isset($call['file'])
                    ? $this->unprefixPath($call['file'])
                    : '[NONE]';
                $line = isset($call['line'])
                    ? "({$call['line']})"
                    : '';

                return "{$file}{$line}: {$call['function']}({$args})";
            })
            ->implode("\n\n")
        ;
    }
}
